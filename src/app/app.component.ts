import { Component } from '@angular/core';
import { BackendApiService } from "./services/backend-api.service";

declare var google:any

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Artivatic.ai';
  States = [];
  Cities = [];
  SelectedValue: any;
  selectedCity : any;

  constructor(public backEndApi : BackendApiService){}

  ngOnInit(){
    this.getApiData();
  }

  getApiData(){
    this.backEndApi.fetchCitiesData().subscribe(prods => {
      console.log(prods);
      for (var i=0;i<prods.length;i++){
         this.States.push(prods[i].State);
      }
      console.log("Lists of State",this.States);
    }, err => {
      console.log('Error', err);
    })
  }

  SelectedName(state){
    console.log(state);
    this.SelectedValue = state;
    this.fetchCitiesBySelectedState(this.SelectedValue);
    this.selectedCity = "";
    var geocoder = new google.maps.Geocoder();
    var mapOptions = {  
      center: {lat: 0, lng: 0},  
      zoom: 10,  
      mapTypeId: google.maps.MapTypeId.ROAD  
    }
    geocoder.geocode({'address': this.SelectedValue}, function(results, status) {
      console.log(results);
      if (status === 'OK') {
        map.setCenter(results[0].geometry.location);
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    }); 
    var map = new google.maps.Map(document.getElementById("map"), mapOptions); 
  }

  SelectedCity(city){
    console.log(city);
    this.selectedCity = city;
    var geocoder = new google.maps.Geocoder();
      var mapOptions = {  
        center: {lat: 0, lng: 0},  
        zoom: 10,  
        mapTypeId: google.maps.MapTypeId.ROAD  
      }
      // geocoder.geocode({'address': this.SelectedValue}, function(results, status) {
      //   console.log(results);
      //   if (status === 'OK') {
      //     map.setCenter(results[0].geometry.location);
      //   } else {
      //     alert('Geocode was not successful for the following reason: ' + status);
      //   }
      // }); 

      var map = new google.maps.Map(document.getElementById("map"), mapOptions); 

      var marker;

      geocoder.geocode({'address': this.selectedCity}, function(results, status) {
        console.log(results[0].geometry.location.lat(),results[0].geometry.location.lng());
        var latlng = (results[0].geometry.location.lat(),results[0].geometry.location.lng());
        if (status === 'OK') {
          map.setCenter(results[0].geometry.location);
          marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: { lat : parseFloat( results[0].geometry.location.lat() ),
              lng : parseFloat( results[0].geometry.location.lng() )}
          });
          //marker.addListener('click', toggleBounce);
        } else {
          alert('Geocode was not successful for the following reason: ' + status);
        }
      });
  }

  fetchCitiesBySelectedState(selected){
    this.backEndApi.fetchCitiesDataByState(selected).subscribe(prods => {
      console.log(prods);
      this.Cities = prods;
    }, err => {
      console.log('Error', err);
    })
  }
}
