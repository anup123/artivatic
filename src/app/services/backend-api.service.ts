import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'; 

@Injectable({
  providedIn: 'root'
})
export class BackendApiService {

  readonly base_url = 'https://indian-cities-api-nocbegfhqg.now.sh/';
  m_Headers : any;
  m_Options : any;

  constructor(public http:Http) {
     console.log(this.base_url);
     this.m_Headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });//x-www-form-urlencoded //application/json'
     this.m_Options = new RequestOptions({ headers: this.m_Headers }); //method : RequestMethod.Post 
   }

   fetchCitiesData(): Observable<any> {
     return this.http.get(this.base_url + 'cities/',this.m_Options).map(res => <any>res.json());
  }

  fetchCitiesDataByState(state):Observable<any> {
    return this.http.get(this.base_url + 'cities?State='+state,this.m_Options).map(res => <any>res.json());
 }
}
